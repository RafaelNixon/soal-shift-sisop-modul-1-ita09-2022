<h1>soal-shift-sisop-modul-1-ITA9-2022</h1>

| No | Nama | NRP |
| --- | --- | --- |
| 1 | Bagus Ridho R | 5027201043 |
| 2 | Fairuz Azhar A | 5027201059 |
| 3 | Rafael Nixon | 05311940000025 |

<h1>Soal 1</h1>
Pembahasan soal no 1 : source [code](https://gitlab.com/mesifer/soal-shift-sisop-modul-1-ita09-2022/-/tree/main/Soal1)

##  Soal register.sh

**Deskripsi Soal**

Membuat sistem register user dengan nama [register.sh](https://gitlab.com/mesifer/soal-shift-sisop-modul-1-ita09-2022/-/blob/main/Soal1/register.sh) dengan ketentuan :

* Minimal 8 karakter
* Memiliki minimal 1 huruf kapital dan 1 huruf kecil
* Alphanumeric
* Tidak boleh sama dengan username

1. Membuat input text username dan password 
```bash
read -p "[username :] " username
read -s -p "[password : ]" password
```
2. Membuat directory ./users/user.txt dan log.txt
```bash
	if [ ! -d ./users/ ] 
	then
		mkdir -p ./users/
		touch ./users/user.txt
	fi

	if [ ! -f ./logg.txt ]
	then
		> logg.txt
	fi
```
3. Periksa apakah username yang dimasukkan sudah terdaftar di dalam file user.txt *flag jika benar. Jika salah message log error ke dalam log.txt
```bash
	while read line
	do
	  	for word in $line
	  	do
	  		if [ "$username" == "$line" ]
			then
					flag=true
					echo -e "\nREGISTER: ERROR user already exist\n"

					echo ""$tgl" "$wkt" REGISTER: ERROR User already exists" >> log.txt
			fi
	  	done
	done < ./users/user.txt
```
4. Jika flag salah / username belum terdaftar maka cek password untuk memenuhi syarat, jika berhasil maka username dan password disimpan ke dalam file user.txt dan message log success ke dalam file log.txt
```bash
	if ! [ "$flag" ]
	then
		if [[ "$len" -ge 8  &&  "$password" =~ [a-z]  &&  "$password" =~ [A-Z] && "$password" =~ `  [0-9] ]]
		then
					echo -e "User $username registered succesfully"
					echo "$username"  >> users/user.txt
					echo "$password"  >> users/user.txt
					echo ""$tgl" "$wkt" REGISTER:INFO User $username registered succesfully" >> log.txt
					exit
		else
			echo -e "\nREGISTER: ERROR Password must be at least 8 CHARACTERS and include UPPERCASE, LOWERCAS, ALPHANUMERIC !!\n"
		fi
	fi	
```
## Soal main.sh

**Deskripsi Soal**

Membuat sistem Login user dengan nama [main.sh](https://gitlab.com/mesifer/soal-shift-sisop-modul-1-ita09-2022/-/blob/main/Soal1/main.sh)

1. buat looping program untuk user menginputkan password dan username
```bash
while [true]
do
	....
done
```
2. membuat input user dan password
```bash
read -p "[Username] : " user
read -s -p "[Password] : " pw
```
3. melakukan pengecekan user dan password yang diinputkan melalui file 'user.txt' dimana setiap baris ganjil merupakan username dan baris genap adalah password. Dan jika data cocok maka user berhasil login dan beri flag=1, cmd=true.
```bash
while read -r odd_line
do

	    username=$odd_line
	    read -r even_line
	    password=$even_line

		 	
	   		if [[ "$username" == "$user" && "$password" == "$pw" ]]
	    	then
	    		echo -e "\nLOGIN: INFO User \"$username\" logged in\n"
				echo ""$tgl" "$wkt" LOGIN: INFO User $username logged in" >> logg.txt

				cmd=true
				flag=1
				att=$((att+1))
				break

	   		fi

			for ((num=1; num<3; num++))
			do
	     		if [ "$user" == "$username" ]
	     		then
	     			echo "LOGIN: ERROR Failed to attempt on user \"$username\""
	     			echo -e "Wrong Password !\n"
				
					flag=1
					fail=$((fail+1))	
					
					echo ""$tgl" "$wkt" LOGIN: ERROR Failed to attempt on user $username" >> logg.txt
					
					break
	     		fi
	     	done
	     if [ "$flag" -eq 1 ]
	     then
	     	break
		 fi
	
	done < ./users/user.txt
```
4. Lalu jika password dan username tidak cocok (flag=0) maka user gagal login
```bash
	if [ "$flag" -eq 0 ]
	then
		echo -e "LOGIN: ERROR User \"$user\" doesn't exist\n"
		fail=$((fail+1))
	fi
```
5. Apabila user berhasil login, maka cmd=true dan keluar loop untuk masuk ke halaman utama
```bash
if [ "$cmd" == true ]
	then
		break
	fi
```
6. looping halaman utama
7. buat input untuk command
```bash
read -p "$ " command N
```
8. jika command = dl, maka unduh file https://loremflickr.com/320/240 sejumlah N ke dalam folder "Y-m-d_username". Setelah unduh, maka zip file tersebut. Lalu, jika mengunduh file kembali maka file zip akan diunzip dan terjadi penambahan file sesuai jumlah yang diunduh.
```bash
if [ "$command" == "dl" ]
	then
		
		mkdir -p ./$(date +"%Y-%m-%d")_$username/
	

		if [ -f ./$(date +"%Y-%m-%d")_$username.zip ]
		then
			unzip -P $password $(date +"%Y-%m-%d")_$username.zip
		fi
		
		for ((i=1; i<=$N; i+=1))
		do

			if [ "$k" -ge 10 ]
			then
				j=$((j+1))
				k=0
				if [ "$j" -ge 10 ]
				then
					j=0
				fi
			fi

			wget -O - https://loremflickr.com/320/240 > ./$(date +"%Y-%m-%d")_$username/PIC_$j$k.jpg

			k=$((k+1))
			
		done

		zip -r --password $password $(date +"%Y-%m-%d")_$username.zip $(date +"%Y-%m-%d")_$username/
```
9. Jika command = att, tampilkan percobaan gagal dan berhasil login
```bash
elif [[ "$command" == "att" && "$N" == "" ]]
	then
		echo  "login fail = $fail"
		echo -e "login success = $att\n"
	fi
```

<h1>Soal 2</h1>

pembasan soal no 2 modul 1

## Solusi dan penjelasan 

disini menggunakan awk, install package awk terlebih dahulu =>[awk](https://packages.ubuntu.com/bionic/awk).
```bash
$ sudo apt install gawk
```
disini kita akan mengecheck folder forensic_log_website daffa apakah sudah ada di document ,apabila sudah akan dibuatkan folder nya di document dengan format nama yang sama setelah program dijalankan

## Pengecheckan file
```bash
#!/bin/bash

if [ ! -d ./forensic_log_website_daffainfo_log ] 
    then
        mkdir ./forensic_log_website_daffainfo_log
    else
        rm -rf ./forensic_log_website_daffainfo_log
	    mkdir ./forensic_log_website_daffainfo_log
fi

```

## Mencari rata rata serangan perjam
code ini diamana ia akan meng extract string masing masin jam dan memasukan nilai dalam array dengan menggunakan increment ++ maka menyimpan banyak array dari ip addres otomatis akan menjumlahkan dan di bagi dengan banyak array tersebut dengan menggunakan loop untuk menghitung avarage perjam ip addres yang mengakses website daffa
  
>awk '{gsub(/"/, "", $1); print $1 }' | awk -F: '{gsub(/:/, " ", $1); arr[$3]++}
>
```pythoncat 
log_website_daffainfo.log | awk '{gsub(/"/, "", $1); print $1 }' | awk -F: '{gsub(/:/, " ", $1); arr[$3]++}
		END {
			for (i in arr) {
				count++
				res+=arr[i]
			}
			res=res/count
			printf "rata-rata serangan tiap jam adalah %.2f request per jam\n\n", res
		}' >> ./forensic_log_website_daffainfo_log/ratarata.txt

```

## Mencari IP addres yang terbanyak dan Curl
> awk '{gsub(/,/, " ", $1); print $1 }' | awk -F: '{gsub(/:/, " ", $1); arr[$1]++}
>
>awk '/curl/ { ++n } // untuk menghitung string Curl dengan awk
>

<h1>Soal 3</h1>
Pembahasan soal no 3
