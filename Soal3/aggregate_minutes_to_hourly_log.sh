#!/bin/bash

dtnow=`date +%Y%m%d%H`
mkdir -p log

# fname=/home/$USER/log/metrics_agg_$dtnow.log

fname=log/metrics_agg_$dtnow.log
touch $fname
chmod 700 $fname

# tofree="total used free shared buff/cache available Mem: "
# cmdfree=`free -m | xargs`
# cmdfrees=${cmdfree/#$tofree/}
# cmdfree=${cmdfrees/Swap: /}
# echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $fname
# cmdfrees=${cmdfree// /,}

# cmddu=`du -sh /home/$USER/ | xargs`
# cmddus=${cmddu% *}
# echo $cmdfrees,/home/$USER/,$cmddus >> $fname

# cmdfind=`find -name /home/$USER/log/"$dtnow*"`
# cmdfind=`grep -Rl stretch log/metrics_$dtnow*`
# echo $cmdfind >> $fname

tname=metrics_20220226140144.log
cd log
files=(script*)
cd ..
echo $files
line=`awk 'END {print $1}' log/$tname`
size=${line##*,}
size=${size%M*}
echo $size >> $fname
