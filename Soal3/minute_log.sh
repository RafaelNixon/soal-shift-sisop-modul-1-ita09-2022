#!/bin/bash

dtnow=`date +%Y%m%d%H%M%S`
mkdir -p log

fname=/home/$USER/log/metrics_$dtnow.log

touch $fname
chmod 700 $fname

tofree="total used free shared buff/cache available Mem: "
cmdfree=`free -m | xargs`
cmdfrees=${cmdfree/#$tofree/}
cmdfree=${cmdfrees/Swap: /}
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $fname
cmdfrees=${cmdfree// /,}

cmddu=`du -sh /home/$USER/ | xargs`
cmddus=${cmddu% *}
echo $cmdfrees,/home/$USER/,$cmddus >> $fname
