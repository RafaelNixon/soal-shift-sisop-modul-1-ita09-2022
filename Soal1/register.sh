#!/usr/bin/bash
	
echo  "=========== REGISTER =============="

while [ true ]

flag=""
tgl=$(date +"%m/%d/%Y/")
wkt=$(date +"%T")

do
	read -p "[username :] " username
	read -s -p "[password : ]" password
	clear
	len=${#password}

	
	if [ ! -d ./users/ ] 
	then
		mkdir -p ./users/
		# mkfile -n  ./users/user.txt
		touch ./users/user.txt
	fi

	if [ ! -f ./log.txt ]
	then
		> log.txt
	fi
	
	while read line
	do
	  	for word in $line
	  	do
	  		if [ "$username" == "$line" ]
			then
					flag=true
					echo -e "\nREGISTER: ERROR user already exist\n"

					echo ""$tgl" "$wkt" REGISTER: ERROR User already exists" >> log.txt
			fi
	  	done
	done < ./users/user.txt

	if ! [ "$flag" ]
	then
		if [[ "$len" -ge 8  &&  "$password" =~ [a-z]  &&  "$password" =~ [A-Z] && "$password" =~ [0-9] ]]
		then
					echo -e "User $username registered succesfully"
					echo "$username"  >> users/user.txt
					echo "$password"  >> users/user.txt
					echo ""$tgl" "$wkt" REGISTER:INFO User $username registered succesfully" >> log.txt
					exit
		else
			echo -e "\nREGISTER: ERROR Password must be at least 8 CHARACTERS and include UPPERCASE, LOWERCAS, ALPHANUMERIC !!\n"
		fi
	fi	
done
